FROM debian:bullseye-slim

COPY ./aklatan /usr/bin/
EXPOSE 3000

CMD ["/usr/bin/aklatan"]
