//go:build mage

package main

import (
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"

	"github.com/bitfield/script"
	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
	"github.com/magefile/mage/target"
)

var (
	commands = []string{"importer"}
	goGlob   = []string{
		"*.go",
		"*/*.go",
		"cmd/*/*.go",
	}
	htmlGlob = []string{
		"templates/*/*.html",
	}
	embedGlob = []string{
		"static/*/*",
		"templates/*/*.html",
	}
)

func All() {
	mg.Deps(Lint)
	mg.Deps(Check)
	mg.Deps(Binary) // equivalent to Makefile's $(PROJECT)
	mg.Deps(Commands)
}

func Lint() {
	output := ".lint"
	newer, err := target.Glob(output, goGlob...)
	if err != nil {
		log.Fatalf("Glob error: %s", err)
	}
	if newer {
		mustRunV("golangci-lint", "run", "--timeout", "180s", "--skip-dirs=rules")
		sh.Run("touch", output)
	}
}

func Check() {
	mg.Deps(dotCoverageDir)

	output := ".coverage/aklatan.out"
	globs := [][]string{goGlob, htmlGlob}
	for _, glob := range globs {
		newer, err := target.Glob(output, glob...)
		if err != nil {
			log.Fatalf("Glob error: %s", err)
		}
		if newer {
			// Note: sh.Exec _would_ substitute $TESTFLAGS, but it won't split it
			// into separate arguments. So for equivalent functionality to the
			// Makefile we have to do the splitting here. (It still might not be
			// exactly 1:1.)
			args := []string{"test", fmt.Sprintf("-coverprofile=%s", output), "."}
			args = append(args, strings.Split(os.Getenv("TESTFLAGS"), " ")...)
			mustRunV("go", args...)
			return
		}
	}
}

func dotCoverageDir() {
	sh.Run("mkdir", "-p", ".coverage")
}

func Cover() {
	mg.Deps(htmlCover)

	// XXX IMO the sed usage in the Makefile is a little clearer, but I went
	// all-out in converting this to "pure" Go by using bitfield/script.
	// However, it's worth noting that calling script.Exec here doesn't provide
	// mage's logging support that we get from sh.RunV.
	total := regexp.MustCompile("^total")
	re := regexp.MustCompile(":.*statements\\)[^0-9]*")
	script.Exec("go tool cover -func .coverage/aklatan.out").MatchRegexp(total).ReplaceRegexp(re, ": ").Stdout()
}

func htmlCover() {
	mg.Deps(Check)

	output := ".coverage/aklatan.html"
	coverOut := ".coverage/aklatan.out"
	newer, err := target.Path(output, coverOut)
	if err != nil {
		log.Fatalf("Path error: %s", err)
	}
	if newer {
		mustRunV("go", "tool", "cover", fmt.Sprintf("-html=%s", coverOut), "-o", output)
	}
}

func Binary() {
	output := "aklatan"
	globs := [][]string{goGlob, embedGlob}
	for _, glob := range globs {
		newer, err := target.Glob(output, glob...)
		if err != nil {
			log.Fatalf("Glob error: %s", err)
		}
		if newer {
			mustRunV("go", "build", ".")
			return
		}
	}
}

func Commands() {
	// This ends up being a nicer structure than the hacky Makefile recipe.
	for _, command := range commands {
		buildCommand(command)
	}
}

func buildCommand(command string) {
	newer, err := target.Glob(command, goGlob...)
	if err != nil {
		log.Fatalf("Glob error: %s", err)
	}
	if newer {
		mustRunV("go", "build", fmt.Sprintf("./cmd/%s", command))
	}
}

// This corresponds to the "report.xml" Makefile target.
func CoverXml() {
	mg.Deps(Check) // XXX this dependency was missing from the Makefile

	output := "report.xml"
	newer, err := target.Glob(output, goGlob...)
	if err != nil {
		log.Fatalf("Glob error: %s", err)
	}
	if newer {
		// XXX sh.RunV vs script.Exec. Beware that script.Exec doesn't seem to have
		// any simple way to implement `pipefail`, and it doesn't seem to expand
		// environment variables that don't exist to an empty string. This makes
		// it so that running it with `$TESTFLAGS` when that variable is unset
		// causes `go test` to fail with an error message, and `go-junit-report` to
		// generate an empty report ... but the overall pipeline has a successful
		// exit status.

		// mustRunV("bash", "-c", "go test $TESTFLAGS -v . 2>&1 | go-junit-report > report.xml")
		_, err = script.Exec("go test -v .").Exec("go-junit-report").WriteFile(output)
		if err != nil {
			log.Fatalf("error writing report.xml: %s", err)
		}
		mustRunV("go", "tool", "cover", "-func", ".coverage/aklatan.out")
	}
}

// mustRunV is a wrapper around sh.RunV that logs a fatal error if an error is
// returned.
func mustRunV(cmd string, args ...string) {
	err := sh.RunV(cmd, args...)
	if err != nil {
		log.Fatalf("error running '%s %v': %s", cmd, args, err)
	}
}

var Default = All
