package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/goyek/goyek/v2"
	"github.com/goyek/x/boot"
	"github.com/goyek/x/cmd"
)

var (
	// This var doesn't have a name because it's not used as a dependency
	// by anything else. We just need the registration side effect.
	_ = goyek.Define(goyek.Task{
		Name:  "semgrep",
		Usage: "semgrep",
		Action: func(a *goyek.A) {
			cmd.Exec(a, "semgrep --config rules/ --metrics=off")
		},
	})

	// This var doesn't have a name because it's not used as a dependency
	// by anything else. We just need the registration side effect.
	_ = goyek.Define(goyek.Task{
		Name:  "report.xml",
		Usage: "go-junit-report",
		Action: func(a *goyek.A) {
			testflags := os.Getenv("TESTFLAGS")
			if !cmd.Exec(a, "go test "+testflags+" -v .") {
				return
			}
		},
		Deps: goyek.Deps{
			check,
		},
	})

	aklatan = goyek.Define(goyek.Task{
		Name:  "aklatan",
		Usage: "go build .",
		Action: func(a *goyek.A) {
			cmd.Exec(a, "go build .")
		},
	})

	check = goyek.Define(goyek.Task{
		Name:  "check",
		Usage: "go test",
		Action: func(a *goyek.A) {
			cmd.Exec(a, "mkdir -p .coverage")

			testflags := os.Getenv("TESTFLAGS")
			if !cmd.Exec(a, "go test "+testflags+" -coverprofile=./.coverage/aklatan.out .") {
				return
			}

			// There's no point in touching the flag file, because goyek
			// doesn't handle file-level dependencies.
			// cmd.Exec(a, "touch .lint")

			cmd.Exec(a, "go tool cover -html=./.coverage/aklatan.out -o ./.coverage/aklatan.html")
		},
	})

	cover = goyek.Define(goyek.Task{
		Name:  "cover",
		Usage: "go tool cover",
		Deps: goyek.Deps{
			check,
		},
		Action: func(a *goyek.A) {
			cmd.Exec(a, "go tool cover -func .coverage/aklatan.out "+
				"| sed -n -e '/^total/s/:.*statements)[^0-9]*/: /p'")
		},
	})

	lint = goyek.Define(goyek.Task{
		Name:  "lint",
		Usage: "golangci-lint",
		Action: func(a *goyek.A) {
			cmd.Exec(a, "golangci-lint run --timeout 180s --skip-dirs=rules")

			// There's no point in touching the flag file, because goyek
			// doesn't handle file-level dependencies.
			// cmd.Exec(a, "touch .lint")
		},
	})
	// This is a placeholder task where individual command dependencies
	// will be attached.
	commands = goyek.Define(goyek.Task{
		Name:  "commands",
		Usage: "commands",
		Deps:  commandDeps(),
	})

	all = goyek.Define(goyek.Task{
		Name:  "all",
		Usage: "build pipeline",
		Deps: goyek.Deps{
			lint,
			check,
			cover, // FIXME: make this optional, and force verbose or log output
			aklatan,
			commands,
		},
	})
)

// commandDeps() returns the list of task dependencies for all commands, by
// reading the ./cmd/ directory for subdirectories. As a side effect, it
// registers tasks for all of those dependencies, using task names of the
// form `command.<command-name>`.
func commandDeps() goyek.Deps {
	dirs, err := ioutil.ReadDir("./cmd/")
	if err != nil {
		log.Fatalf("error reading command dir: %s", err)
	}

	commandDeps := goyek.Deps{}
	for _, dir := range dirs {
		if !dir.IsDir() {
			continue
		}

		taskName := fmt.Sprintf("command.%s", dir.Name())
		defTask := goyek.Define(goyek.Task{
			Name:  taskName,
			Usage: taskName,
			Action: func(a *goyek.A) {
				cmd.Exec(a, fmt.Sprintf("go build ./cmd/%s", dir.Name()))
			},
		})

		commandDeps = append(commandDeps, defTask)
	}

	return commandDeps
}

func main() {
	goyek.SetDefault(all)
	boot.Main()
}
